import { setupEnvironment } from "cypress/support/integration-setup";
import { teardownEnvironment } from "cypress/support/integration-teardown";

const HOST = Cypress.env('HOST');

describe("Response Performance Time", () => {
    setupEnvironment();

    it('Create 10 Addresses', () => {
        const merchant_token: string = Cypress.env('merchant_token');
        const magic_numbers: string[] = ['5005559999', '5005559998', '5005559997', '5005559996', '5005559995', '5005559994', '5005559993', '5005559992', '5005559991', '5005559990'];

        let responseTimes: number[] = [];
        let average: number = 0;

        for (let i = 0; i < magic_numbers.length; i++) {
            cy.request({
                method: 'POST',
                url: `https://${HOST}/v3/addresses`,
                headers: {
                    "Authorization": `Bearer ${merchant_token}`
                },
                body: {
                    "value": `+1${magic_numbers[i]}`
                }
            }).then(response => {
                expect(response.status).to.eq(201)
                responseTimes.push(response.duration)

                if (responseTimes.length === 10) {
                    average = Math.floor(responseTimes.reduce((a, b) => a + b, 0) / responseTimes.length)
                    Cypress.env('average', average)
                }
            });
        }
    });

    it('Validates average response time less than 2000s', () => {
        cy.log(`Average response time: ${Cypress.env('average')} ms`);
        expect(Number(Cypress.env('average'))).to.be.lessThan(2000); // less than 2 seconds
    });

    it('Performs Teardown Sequence', () => {
        teardownEnvironment();
    });
})