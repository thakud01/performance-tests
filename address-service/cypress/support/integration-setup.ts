import { generateToken, createApplication, createMerchant, deleteApplication } from './functions';

const APP_CLIENT_SECRET: string = Cypress.env('APP_CLIENT_SECRET');
const APP_CLIENT_ID: string = Cypress.env('APP_CLIENT_ID');

export const setupEnvironment = () => {
    before(() => {
        // token as application
        const role: string = "system";
        const scope: string = "applications:create applications:secret";

        generateToken(APP_CLIENT_ID, APP_CLIENT_SECRET, scope, role).then((token) => {
            Cypress.env("app_token", token);
            cy.wrap(token).should('not.be.empty');
        });

        // system application
        cy.then(() => {
            const role: string = "system";
            const description: string = "Cypress System Application";
            const scope: string[] = ["applications:create", "applications:secret"];
            const app_token: string = Cypress.env('app_token');

            createApplication(app_token, description, scope, role).then((application) => {
                Cypress.env("system_client_id", application.id);
                expect(application.id).to.not.be.undefined;
            });
        })

        // partner application
        cy.then(() => {
            const role: string = "partner";
            const description: string = "Cypress Partner Application";
            const scope: string[] = ["merchants:create"];
            const app_token: string = Cypress.env('app_token');

            createApplication(app_token, description, scope, role).then((application) => {
                Cypress.env("partner_client_id", application.id);
                Cypress.env("partner_client_secret", application.sharedSecret);
                expect(application.id).to.not.be.undefined;
                expect(application.sharedSecret).to.not.be.undefined;
            });
        });

        // token as partner
        cy.then(() => {
            const clientId: string = Cypress.env('partner_client_id');
            const clientSecret: string = Cypress.env('partner_client_secret');
            const scope: string = "merchants:create";
            const role: string = "partner";

            generateToken(clientId, clientSecret, scope, role).then((token) => {
                Cypress.env("partner_token", token);
                expect(token).to.not.be.undefined;
                cy.wrap(token).should('not.be.empty');
            });
        });

        // create merchant
        cy.then(() => {
            const partner_token = Cypress.env('partner_token');
            const merchantName: string = `Merchant ${Math.floor(Math.random() * 1000)}`;

            createMerchant(partner_token, merchantName).then(bp_id => {
                Cypress.env('merchant_id', bp_id);
            });
        });

        // create merchant application
        cy.then(() => {
            const description: string = "Cypress Merchant Application";
            const role: string = "merchant";
            const scope: string[] = ["addresses:create"];
            const app_token: string = Cypress.env('app_token');

            createApplication(app_token, description, scope, role).then((application) => {   
                Cypress.env("merchant_client_id", application.id);
                Cypress.env("merchant_client_secret", application.sharedSecret);
                expect(application.id).to.not.be.undefined;
                expect(application.sharedSecret).to.not.be.undefined;
            });
        });

        // token as merchant
        cy.then(() => {
            const clientId: string = Cypress.env('merchant_client_id');
            const clientSecret: string = Cypress.env('merchant_client_secret');
            const scope: string = "addresses:create";
            const role: string = "merchant";

            generateToken(clientId, clientSecret, scope, role).then((token) => {
                Cypress.env("merchant_token", token);
                expect(token).to.not.be.undefined;
                cy.wrap(token).should('not.be.empty');
            });
        });
    });
}