export interface tokenPayload {
    client_id: string,
    expiration: string,
    signature_value: string,
    sub: string,
    timestamp: number,
    signature: string,
    audience: string,
    scope: string,
    role?: string,
    partnerId: string,
    merchantId?: string
}

export interface applicationPayload {
    description: string,
    defaultRole?: string, // optional for system
    partnerId?: string, // optional for system
    merchantId?: string, // optional for system & partner
    expiration: {
        default: string,
        maximum: string
    },
    scope: string[],
    allowedAudiences: string[]
}