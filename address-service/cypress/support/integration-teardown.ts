import { generateToken, deleteApplication } from './functions';

const APP_CLIENT_ID = Cypress.env('APP_CLIENT_ID');
const APP_CLIENT_SECRET = Cypress.env('APP_CLIENT_SECRET');

export const teardownEnvironment = () => {
    // token as application
    const role = "system";
    const scope = "applications:delete";

    generateToken(APP_CLIENT_ID, APP_CLIENT_SECRET, scope, role).then((token) => {
        Cypress.env("app_token", token);
    });

    // delete system application
    cy.then(() => {
        const appToken = Cypress.env('app_token');
        const clientId = Cypress.env('system_client_id');

        deleteApplication(appToken, clientId).then(response => {
            expect(response.status).to.eq(204);
            console.log('Application deleted');
        })
    })
}