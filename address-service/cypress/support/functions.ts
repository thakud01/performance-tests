const uuid = require('uuid');
const CryptoJS = require('crypto-js');
import { tokenPayload, applicationPayload } from './interfaces';

const ENVIRONMENT: string = Cypress.env('ENVIRONMENT');
const HOST: string = Cypress.env('HOST');
const partner_id: string = Cypress.env('partner_id');
const merchant_id: string = Cypress.env('merchant_id');

// generates a token
export function generateToken(clientId: string, clientSecret: string, scope: string, role: string) {
    const signature_value = uuid.v4();
    const timestamp = Math.floor(Date.now() / 1000);
    const hmac = CryptoJS.HmacSHA256(`${signature_value}.${signature_value.length}.${timestamp}`, clientSecret);
    const encodedSource = CryptoJS.enc.Base64.stringify(hmac);

    const tokenPayload: tokenPayload = {
        "client_id": clientId,
        "expiration": "30m",
        "signature_value": signature_value,
        "sub": role,
        "timestamp": timestamp,
        "signature": encodedSource,
        "audience": 'integration.api.authvia.com/v3',
        "scope": scope,
        "partnerId": partner_id,
    };

    addTokenFields(tokenPayload, role);

    return cy.request({
        method: 'POST',
        url: `https://${HOST}/v3/tokens`,
        body: tokenPayload,
        failOnStatusCode: false
    }).then(response => {
        return response.body.token;
    });
}

// creates an application
export function createApplication(token: string, description: string, scope: string[], role: string) {
    const applicationPayload: applicationPayload = {
        "description": description,
        "expiration": {
            "default": "1h",
            "maximum": "1d"
        },
        "scope": scope,
        "allowedAudiences": [`${ENVIRONMENT}.api.authvia.com/v3`]
    };

    addApplicationFields(applicationPayload, role);

    return cy.request({
        method: 'POST',
        url: `https://${HOST}/v3/applications`,
        body: applicationPayload,
        headers: {
            'Authorization': `Bearer ${token}`
        },
        failOnStatusCode: false
    }).then(response => {
        expect(response.status).to.eq(201);
        expect(response.body).to.contain.keys('id', 'sharedSecret');
        Cypress.env('id', response.body.id);
        Cypress.env('sharedSecret', response.body.sharedSecret);
        return { id: response.body.id, sharedSecret: response.body.sharedSecret };
    });
}

// deletes an application
export function deleteApplication(token: string, clientId: string) {
    return cy.request({
        method: 'DELETE',
        url: `https://${HOST}/v3/applications/${clientId}`,
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }).then(response => {
        expect(response.status).to.eq(204);
        return response;
    });
}

// create a merchant
export function createMerchant(token: string, name: string) {
    return cy.request({
        method: 'POST',
        url: `https://${HOST}/v3/merchants`,
        headers: {
            'Authorization': `Bearer ${token}`
        },
        body: {
            "profile": {
                "name": name
            }
        }
    }).then(response => {
        expect(response.status).to.eq(201);
        return response.body.id;
    })
}

/********** Helper Functions **********/
// Add fields to token generator, depending on the role provided
function addTokenFields(tokenPayload: tokenPayload, role: string) {
    switch (role) {
        case "system":
            break;
        case "partner":
            tokenPayload.partnerId = partner_id;
            break;
        case "merchant":
            tokenPayload.merchantId = `${Cypress.env('merchant_id')}`;
            break;
        default:
            throw new Error(`${role} does not exist`);
    }
}

// Add fields to application creator, depending on the role provided
function addApplicationFields(applicationPayload: applicationPayload, role: string) {
    switch(role) {
        case "system":
            applicationPayload.defaultRole = "system";
            applicationPayload.partnerId = partner_id;
            break;
        case "partner":
            applicationPayload.defaultRole = "partner";
            applicationPayload.partnerId = partner_id;
            break;
        case "merchant":
            applicationPayload.defaultRole = "merchant";
            applicationPayload.partnerId = partner_id;
            applicationPayload.merchantId = `${Cypress.env('merchant_id')}`;
    }
}