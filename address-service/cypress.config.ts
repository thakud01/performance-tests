const { defineConfig } = require("cypress");

module.exports = defineConfig({
  reporter: 'cypress-mochawesome-reporter',
  reporterOptions: {
    reportFilename: '[name]-[timestamp]',
    reportDir: './reports',
    saveJson: true,
    saveHtml: false
  },
  e2e: {
    setupNodeEvents(on, config) {
      require('cypress-mochawesome-reporter/plugin')(on);
    },
    screenshotOnRunFailure: false,
    specPattern: "testing/**/*.cy.{js,jsx,ts,tsx}",
    video: false
  },
  env: {
    HOST: process.env.HOST,
    ENVIRONMENT: process.env.ENVIRONMENT,
    APP_CLIENT_ID: process.env.APP_CLIENT_ID,
    APP_CLIENT_SECRET: process.env.APP_CLIENT_SECRET,
    partner_id: process.env.partner_id
  },
  pageLoadTimeout: 120000, // increased from default of 60s
  defaultCommandTimeout: 10000 // increased from default of 4s
});
